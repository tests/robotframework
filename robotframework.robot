*** Settings ***
Documentation
...    This Robot Framework script demonstrates various functionalities
...    including transferring files using SCP,
...    serial communication, and executing commands over SSH.
...    It utilizes SSHLibrary, Process, Collections, OperatingSystem, and SerialLibrary.

Library            SSHLibrary    WITH NAME    SSH
Library            Process
Library            OperatingSystem
Library            SerialLibrary    WITH NAME    Serial

*** Variables ***
${target}
${username}        user
${password}        user

*** Test Cases ***
Robotframework SCP test
    [Documentation]
    ...    Test SCP operations - creating a file, copying it to a target using SCP, and checking integrity.
    Create File     ${CURDIR}/example.txt    Hello World!
    Log To Console    ${\n}example.txt file is created on target current directory!
    Open Connection     ${target}        
    SSH.Login           ${username}    ${password}  
    SSH.Put File        ${CURDIR}/example.txt    /home/user
    Log To Console    Copied example.txt file from target to target at /home/user!
    ${cmd_out}    SSH.Execute Command    cat /home/user/example.txt
    IF    '${cmd_out}' == 'Hello World!'
    Log To Console    Integrity check: Copied content is the same on Target
    ELSE
    Log To Console    Integrity check: Copied content is not the same on Target
    END
    Close All Connections

Robotframework serial test 
    [Documentation]
    ...    Test serial communication using SerialLibrary.
    Serial.Add Port    loop://    timeout=0.1
    ${bytes} =    Set Variable    010203040506070809ABCDEF
    Serial.Write Data    01 23 45 0A 67 89 AB CD EF
    ${read} =    Serial.Read Until
    Should Be Equal As Strings    ${read}    01 23 45 0A
    ${read} =    Serial.Read Until   size=2
    Should Be Equal As Strings    ${read}    67 89
    ${read} =    Serial.Read Until   terminator=CD
    Should Be Equal As Strings    ${read}    AB CD
    ${read} =    Serial.Read Until
    Should Be Equal As Strings    ${read}    EF
    ${read} =    Serial.Read Until
    Should Be Equal As Strings    ${read}    ${EMPTY}
    Serial.Delete All Ports

Test SSH Connection
    [Documentation]
    ...    Test establishing an SSH connection, executing commands, and checking the output.
    Open Connection     ${target}
    SSH.Login               ${username}    ${password}
    SSH.Write     uname -a      
    SSH.Set Client Configuration    prompt=$
    ${cmd_out} =    SSH.Read Until    $
    ${cmd_exe}=      SSH.Execute Command    /sbin/ifconfig
    Log                ${cmd_exe}
    log to console    ${\n}${cmd_exe}${\n}
    log to console  ${\n}${cmd_out}${\n}${\n}
    Should Not Contain    ${cmd_out}    ${target}
    ${pwd_out}=        SSH.Execute Command    pwd_out
    Log            ${pwd_out}
    log to console  ${\n}${pwd_out}${\n}${\n}
    Close All Connections
